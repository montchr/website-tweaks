// ==UserScript==
// @name         devenv.sh docs toc readability
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  Workaround for https://github.com/cachix/devenv/issues/477
// @author       Chris Montgomery <chris@cdom.io>
// @match        https://devenv.sh/reference/options/
// @icon         https://icons.duckduckgo.com/ip2/devenv.sh.ico
// @grant        none
// @license      GPL-3.0-or-later
// ==/UserScript==

(function() {
  'use strict'

  document.querySelectorAll('.md-ellipsis').forEach((el) => {
    const text = el.innerHTML
    el.innerHTML = text.replaceAll(/.46./g, '.')
  })
})();
